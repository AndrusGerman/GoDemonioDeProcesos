package main

import (
	"strconv"

	"github.com/360EntSecGroup-Skylar/excelize"
)

func saveXLXS(data Formato, sheet string, file *excelize.File) {
	_, ultimo := recorrecXLSXFile(sheet, file)
	numero := strconv.Itoa(int(ultimo + 1))
	file.SetCellStr(sheet, "A"+numero, data.A)
	file.SetCellStr(sheet, "B"+numero, data.B)
	file.SetCellStr(sheet, "C"+numero, data.C.(string))
	file.Save()
}
