package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

func main() {}
func init() {
	gin.SetMode(gin.ReleaseMode)
}

// Auth server Handler
func Auth() gin.HandlerFunc {
	return gin.BasicAuth(gin.Accounts{
		"example": "example",
	})
}

// RunServer in tls or http
func RunServer(port string, server *gin.Engine, tls bool) {
	if tls {
		fmt.Println("SERVER-TLS: ",
			server.RunTLS(":"+port, "/opt/psa/var/certificates/scfp3rfy8", "/opt/psa/var/certificates/scfp3rfy8"),
		)
	} else {
		fmt.Println("SERVER: ",
			server.Run(":"+port),
		)
	}

}
