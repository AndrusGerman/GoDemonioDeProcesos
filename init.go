package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize"
)

func init() {
	initFlags()
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		<-sigchan
		fmt.Println("Byeee")
		stopApss(printLog)
		os.Exit(0)
	}()
}
func initFileXLSX(fichero string, funcion func() *excelize.File, save bool) *excelize.File {
	data := new(excelize.File)
	if _, err := os.Stat(fichero); os.IsNotExist(err) {
		data = funcion()
	} else {
		data, err = excelize.OpenFile(fichero)
		if err != nil {
			fmt.Println("Error Fichero: ", err)
			os.Exit(1)
		}
	}
	if save {
		data.SaveAs(fichero)
	}
	return data
}

func initXLSXProcesos() *excelize.File {
	xlsxNew := excelize.NewFile()
	index := xlsxNew.NewSheet(sheetProcesos)
	xlsxNew.SetActiveSheet(index)
	xlsxNew.SetCellValue(sheetProcesos, "A1", "NombreProceso")
	xlsxNew.SetCellValue(sheetProcesos, "B1", "Folder")
	xlsxNew.SetCellValue(sheetProcesos, "C1", "Running")

	xlsxNew.SetCellValue(sheetUpdate, "A1", "Folder")
	xlsxNew.SetCellValue(sheetUpdate, "B1", "Update Comand")
	xlsxNew.SetCellValue(sheetUpdate, "B1", "Build Comand")
	xlsxNew.DeleteSheet("Sheet1")
	xlsxNew.SaveAs(ficheroProcesos)
	return xlsxNew
}

func initXLSXLog() *excelize.File {
	xlsxNew := excelize.NewFile()
	index := xlsxNew.NewSheet(sheetLog)
	xlsxNew.SetActiveSheet(index)
	xlsxNew.SetCellValue(sheetLog, "A1", "Tiempo")
	xlsxNew.SetCellValue(sheetLog, "B1", "Tipo")
	xlsxNew.SetCellValue(sheetLog, "C1", "Salida")
	xlsxNew.DeleteSheet("Sheet1")
	xlsxNew.SaveAs(ficheroLog)
	return xlsxNew
}

func initFlags() {
	var inter int
	flag.IntVar(&inter, "inter", 0, "Intervalor de Cada escaneo")
	flag.BoolVar(&logDebugPrint, "debug", false, "Print Log, For Debug")
	flag.Parse()
	if inter > 200 {
		intervalo = (time.Millisecond * time.Duration(inter))
	}
}
