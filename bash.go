package main

import (
	"fmt"
	"os/exec"
)

func bashRun(comand string) (string, error) {
	salid, err := exec.Command("bash", "-c", comand).Output()
	printLog(comand)
	salida := string(salid)
	return salida, err
}
func bashRunLog(comand string) (string, error) {
	salida, err := bashRun(comand)
	if err != nil {
		saveLog("Error: "+comand, err.Error())
	}
	saveLog("Salida: "+comand, salida)
	return salida, err
}

func bashInFolder(folder string, comand string) string {
	salida, _ := bashRun(fmt.Sprintf("cd %s;%s", folder, comand))
	return salida
}
func bashInFolderLog(folder string, comand string) string {
	salida, _ := bashRunLog(fmt.Sprintf("cd %s;%s", folder, comand))
	return salida
}
