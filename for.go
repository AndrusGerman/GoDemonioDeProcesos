package main

import (
	"strconv"

	"github.com/360EntSecGroup-Skylar/excelize"
)

func recorrecXLSXFile(sheet string, file *excelize.File) (formato []Formato, ultimo uint) {
	var errores = 0
	var indice uint = 2
	for {
		numero := strconv.Itoa(int(indice))
		A, _ := file.GetCellValue(sheet, "A"+numero)
		B, _ := file.GetCellValue(sheet, "B"+numero)
		C, _ := file.GetCellValue(sheet, "C"+numero)
		if A == "" {
			errores++
			if errores == 4 {
				break
			}
		} else {
			ultimo = indice
			formato = append(formato, Formato{
				A:      A,
				B:      B,
				C:      C,
				Indice: indice,
			})
		}
		indice++
	}
	return formato, ultimo
}
