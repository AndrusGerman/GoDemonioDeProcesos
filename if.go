package main

import (
	"errors"
)

func isRunningProcess(comand string) (bool, error) {
	commando := "ps -ef | awk '$8==\"" + comand + "\" {print $2}'"
	salida, _ := bashRun(
		commando,
	)
	printLog("IsRunning: " + commando + " Salida = " + salida)
	if salida == "" {
		return false, errors.New("No encontrado")
	}
	return true, nil
}
