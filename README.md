# Go Demonio de Procesos
GoDemonio. Es monitor de procesos escrito en Go

## Required Packages:
- Forever
    * ["Xlsx"](https://github.com/360EntSecGroup-Skylar/excelize)
    * ["Server"](https://github.com/gin-gonic/gin)

## Reports:
- [![Go Report Card](https://goreportcard.com/badge/gitlab.com/AndrusGerman/GoDemonioDeProcesos)](https://goreportcard.com/report/gitlab.com/AndrusGerman/GoDemonioDeProcesos)



