package main

import (
	"time"

	"github.com/360EntSecGroup-Skylar/excelize"
)

// Procesos
var xlsxApps *excelize.File
var sheetProcesos = "apps"
var ficheroProcesos = "./files/procesos.xlsx"
var procesosEnable = true

// Updata
var sheetUpdate = "update"

//Log Data
var xlsxLog *excelize.File
var sheetLog = "logs"
var ficheroLog = "./files/log.xlsx"

// App vars
var intervalo = (time.Millisecond * 1234)
var logDebugPrint bool

// PrintlnLOG print log
var PrintlnLOG func(string, ...interface{})

// PrintlnERROR print log
var PrintlnERROR func(string, ...interface{})
