package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("APP: Iniciando")
	serverDemonio()
	xlsxLog = initFileXLSX(ficheroLog, initXLSXLog, true)
	for {
		time.Sleep(intervalo)
		runAllApps()
	}
}

func runAllApps() {
	if !procesosEnable {
		return
	}
	for _, comand := range getApps() {
		if !procesosEnable {
			return
		}
		salida, _ := isRunningProcess(comand.NameProcess)
		if salida == false {
			fmt.Printf("App= %s: Run='%s'\n", comand.NameProcess, comand.Running)
			go bashInFolderLog(comand.Folder, comand.Running)
		}
	}
}
