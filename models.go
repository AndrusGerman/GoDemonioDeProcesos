package main

// Formato xlsx
type Formato struct {
	Indice uint
	A      string
	B      string
	C      interface{}
}

// AppsRunning model
type AppsRunning struct {
	NameProcess string
	Running     bool
}
