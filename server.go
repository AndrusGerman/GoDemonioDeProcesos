package main

import (
	"fmt"
	"net/http"
	"os"
	"plugin"

	"github.com/gin-gonic/gin"
)

func serverDemonio() {
	// Inicia el plugin
	servePlugin, err := plugin.Open("./lib/server/server.so")
	if err != nil {
		fmt.Printf("SERVER: No iniciado (%v)\n", err)
		return
	}
	authFunc, err := servePlugin.Lookup("Auth")
	if err != nil {
		fmt.Println("SERVER: No iniciado (Funcion Auth No encontrada)")
		return
	}
	RunServerPlugin, err := servePlugin.Lookup("RunServer")
	if err != nil {
		fmt.Println("SERVER: No iniciado (Funcion RunServer No encontrada)")
		return
	}
	fmt.Println("SERVER: Iniciando")
	var authHandler = authFunc.(func() gin.HandlerFunc)()
	// Server
	gin.SetMode(gin.ReleaseMode)
	server := gin.New()
	auth := server.Group("/", authHandler)
	auth.GET("/", func(ctx *gin.Context) {
		ctx.JSON(200, gin.H{
			"WelcomeDaemon":               "Hello from 'GoDemonioDeProcesos'",
			"RunningMonitorMachineEnable": procesosEnable,
			"ProcesosRunningMachine":      allAppsRunning(),
			"rutas": gin.H{
				"PauseScanApps":       "/pause_apps_scan",
				"ReanudeAppsScan":     "/reanude_apps_scan",
				"StopAllApps":         "/stop_apps",
				"UpdateAllApps":       "/update_apps",
				"KillDemonio":         "/kill",
				"LogsDemonio":         "/logs",
				"StopAppsAndPostgres": "/stop_postgres",
				"StartPostgres":       "/start_postgres",
			},
		})
	})
	auth.GET("/pause_apps_scan", func(ctx *gin.Context) {
		procesosEnable = false
		ctx.JSON(200, gin.H{"response": "complete"})
	})
	auth.GET("/reanude_apps_scan", func(ctx *gin.Context) {
		procesosEnable = true
		ctx.JSON(200, gin.H{"response": "complete"})
	})
	auth.GET("/stop_apps", func(ctx *gin.Context) {
		stopApss(printLog)
		ctx.JSON(200, gin.H{"response": "complete"})
	})
	auth.GET("/stop_postgres", func(ctx *gin.Context) {
		stopApss(printLog)
		var salida = stopPostgres()
		if salida == "" {
			ctx.JSON(200, gin.H{"response": "complete"})
		} else {
			ctx.JSON(200, gin.H{"response": salida})
		}
	})
	auth.GET("/start_postgres", func(ctx *gin.Context) {
		var salida = startPostgres()
		if salida == "" {
			ctx.JSON(200, gin.H{"response": "complete"})
		} else {
			ctx.JSON(200, gin.H{"response": salida})
		}
	})

	auth.GET("/kill", func(ctx *gin.Context) {
		procesosEnable = false
		ctx.JSON(200, gin.H{"response": "Bye"})
		go func() {
			stopApss(printLog)
			os.Exit(0)
		}()
	})
	auth.GET("/logs", func(ctx *gin.Context) {
		respuesta, _ := recorrecXLSXFile(sheetLog, xlsxLog)
		ctx.JSON(200, gin.H{"response": respuesta})
	})
	auth.GET("/update_apps", func(ctx *gin.Context) {
		ctx.Writer.Header().Set("content-type", "application/json; charset=utf-8")
		ctx.Writer.WriteHeader(http.StatusOK)
		updateApps(ctx)
	})
	go RunServerPlugin.(func(port string, server *gin.Engine, tls bool))("8096", server, false)
}

// MessagePush in server
func MessagePush(ctx *gin.Context, message string) {
	ctx.Writer.WriteString(message)
	ctx.Writer.Flush()
}

func stopPostgres() string {
	salida, _ := bashRun("systemctl stop postgresql-10")
	return salida
}

func startPostgres() string {
	salida, _ := bashRun("systemctl start postgresql-10")
	return salida
}
