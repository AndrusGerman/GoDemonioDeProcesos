package main

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
)

func updateApps(ctxServer *gin.Context) {
	var compilarApp = make(map[string]bool)
	var defaultprocesosEnable = procesosEnable
	// Stop Apps
	stopApss(
		func(log string) {
			MessagePush(ctxServer, log)
		},
	)
	// Stop postgres
	MessagePush(ctxServer, fmt.Sprintf("PostgresStop: %v\n", len(stopPostgres()) < 2))
	// Read apps XLSX
	datos, _ := recorrecXLSXFile(sheetUpdate, xlsxApps)
	// Update
	for _, up := range datos {
		salidaUpdate := bashInFolder(up.A, up.B)
		compilarApp[up.A] = !strings.Contains(salidaUpdate, "Already up-to-date.")
		MessagePush(ctxServer, "Update: F= "+up.A+" SALIDA= "+salidaUpdate)
	}
	// Build
	for _, up := range datos {
		var message = "\nBuild: F= " + up.A
		if compilarApp[up.A] == true {
			var salida = bashInFolder(up.A, up.C.(string))
			if len(salida) > 3 {
				message += ", Salida = \n" + salida
			}
			MessagePush(ctxServer, message)
		} else {
			//MessagePush(ctxServer, message+", Salida 'Already build'")
		}
	}
	// Start Postgres
	MessagePush(ctxServer, fmt.Sprintf("\nPostgresStart: %v\n\n", len(startPostgres()) < 2))
	procesosEnable = defaultprocesosEnable
	MessagePush(ctxServer, "\nSTART: All apps")
}
