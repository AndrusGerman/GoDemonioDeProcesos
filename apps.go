package main

import (
	"fmt"
	"strings"
	"time"
)

func allAppsRunning() (salida []AppsRunning) {
	var defaultprocesosEnable = procesosEnable
	procesosEnable = false
	time.Sleep(intervalo / 3)
	for _, comand := range getApps() {
		salidaRunning, _ := isRunningProcess(comand.NameProcess)
		salida = append(salida, AppsRunning{NameProcess: comand.NameProcess, Running: salidaRunning})
	}
	procesosEnable = defaultprocesosEnable
	return salida
}

func stopApss(call func(log string)) (retorna string) {
	procesosEnable = false
	time.Sleep(intervalo + 400)
	for _, comand := range getApps() {
		salidaRunning, _ := isRunningProcess(comand.NameProcess)
		if salidaRunning {
			killname := strings.Replace(comand.NameProcess, "./", "", 1)
			bashRun(fmt.Sprintf("killall -15 %s", killname))
			salidaRunning, _ = isRunningProcess(comand.NameProcess)
			call(fmt.Sprintf("STOP = %s: SUCCESS = %v\n", comand.NameProcess, !salidaRunning))
		}
	}
	return retorna
}
