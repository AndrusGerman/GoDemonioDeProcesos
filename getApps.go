package main

// AppApps structure
type AppApps struct {
	NameProcess string
	Folder      string
	Running     string
}

func getApps() []AppApps {
	var salida []AppApps
	xlsxApps = initFileXLSX(ficheroProcesos, initXLSXProcesos, false)
	datas, _ := recorrecXLSXFile(sheetProcesos, xlsxApps)
	for _, valor := range datas {
		salida = append(salida, AppApps{
			NameProcess: valor.A,
			Folder:      valor.B,
			Running:     valor.C.(string),
		})
	}
	return salida
}
